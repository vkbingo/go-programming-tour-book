package dao

import "blog-service/blog-service/internal/model"

func (d *Dao) GetArticleTagByAID(articleID uint32) ([]model.ArticleTag, error) {
	articleTag := model.ArticleTag{ArticleID: articleID}
	return articleTag.GetByAID(d.engine)
}

func (d *Dao) GetArticleTagListByTID(tagID uint32) ([]*model.ArticleTag, error) {
	articleTag := model.ArticleTag{TagID: tagID}
	return articleTag.ListByTID(d.engine)
}

func (d *Dao) GetArticleTagListByAIDs(articleIDs []uint32) ([]*model.ArticleTag, error) {
	articleTag := model.ArticleTag{}
	return articleTag.ListByAIDs(d.engine, articleIDs)
}

func (d *Dao) CreateArticleTag(articleID uint32, tagID []uint32, createBy string) error {
	for _, val := range tagID {
		articleTag := model.ArticleTag{TagID: val, ArticleID: articleID, Model: &model.Model{CreatedBy: createBy}}
		if err := articleTag.Create(d.engine); err != nil {
			return err
		}
	}
	return nil
}

func (d *Dao) UpdateArticleTag(articleID uint32, tagID []uint32, modifiedBy string) error {
	articleTag := model.ArticleTag{ArticleID: articleID}
	if err := articleTag.Delete(d.engine); err != nil {
		return err
	}

	for _, tag := range tagID {
		articleTag1 := model.ArticleTag{TagID: tag, ArticleID: articleID, Model: &model.Model{ModifiedBy: modifiedBy}}
		if err := articleTag1.Create(d.engine); err != nil {
			return err
		}
	}

	return nil
}

func (d *Dao) DeleteArticleTag(articleID uint32) error {
	articleTag := model.ArticleTag{ArticleID: articleID}
	return articleTag.Delete(d.engine)
}
