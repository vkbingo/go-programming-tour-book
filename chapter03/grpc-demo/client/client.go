package main

import (
	"context"
	"flag"
	"fmt"
	pb "github.com/go-programming-tour-book/grpc-demo/proto"
	"google.golang.org/grpc"
	"io"
	"log"
)

var port string

func init() {
	flag.StringVar(&port, "port", "8004", "启动端口号")
	flag.Parse()
}

func main() {
	conn, _ := grpc.Dial(":"+port, grpc.WithInsecure())
	defer conn.Close()

	//client := pb.NewGreeterClient(conn)
	//client := pb.NewGreeter1Client(conn)
	//client := pb.NewGreeter2Client(conn)
	client := pb.NewGreeter3Client(conn)
	//_ = SayHello(client)
	//_ = SayList(client, &pb.HelloRequest{Name: "allin"})
	//_ = SayRecord(client, &pb.HelloRequest{Name: "allin"})
	_ = SayRoute(client, &pb.HelloRequest{Name: "alex"})
}

func SayHello(client pb.GreeterClient) error {
	resp, _ := client.SayHello(context.Background(), &pb.HelloRequest{Name: "allin"})
	log.Printf("client.SayHello resp: %s", resp.Message)
	return nil
}

func SayList(client pb.Greeter1Client, r *pb.HelloRequest) error {
	stream, _ := client.SayList(context.Background(), r)
	for {
		resp, err := stream.Recv()
		if err == io.EOF {
			fmt.Println("---end")
			break
		}
		if err != nil {
			return err
		}
		log.Printf("resp: %v", resp)
	}

	return nil
}

func SayRecord(client pb.Greeter2Client, r *pb.HelloRequest) error {
	stream, _ := client.SayRecord(context.Background())
	for n := 0; n < 6; n++ {
		_ = stream.Send(r)
	}
	resp, _ := stream.CloseAndRecv()

	log.Printf("resp err: %v", resp)
	return nil
}

func SayRoute(client pb.Greeter3Client, r *pb.HelloRequest) error {
	stream, _ := client.SayRoute(context.Background())
	for n := 0; n <= 6; n++ {
		_ = stream.Send(r)
		resp, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		log.Printf("resp err: %v", resp)
	}
	_ = stream.CloseSend()
	return nil
}
