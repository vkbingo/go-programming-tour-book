package service

import (
	"blog-service/blog-service/internal/dao"
	"blog-service/blog-service/internal/model"
	"blog-service/blog-service/pkg/app"
	"blog-service/blog-service/pkg/convert"
	"strings"
)

type ArticleRequest struct {
	ID    uint32 `form:"id" binding:"required,gte=1"`
	State uint8  `form:"state,default=1" binding:"oneof=0 1"`
}

type ArticleListRequest struct {
	TagID []uint32 `form:"tag_id" binding:"gte=1"`
	State uint8    `form:"state, default=1" binding:"oneof=0 1"`
}

type CreateArticleRequest struct {
	TagID         string `form:"tag_id" binding:"required,gte=1"`
	Title         string   `form:"title" binding:"required,min=2,max=100"`
	Descr         string   `form:"descr" binding:"required,min=2,max=255"`
	Content       string   `form:"content" binding:"required,min=2,max=4294967295"`
	CoverImageUrl string   `form:"cover_image_url" binding:"required,url"`
	CreateBy      string   `form:"create_by" binding:"required,min=2,max=100"`
	State         uint8    `form:"state,default=1" binding:"oneof=0 1"`
}

type UpdateArticleRequest struct {
	ID            uint32   `form:"id" binding:"required,gte=1"`
	TagID         []uint32 `form:"tag_id" binding:"required,gte=1"`
	Title         string   `form:"title" binding:"min=2,max=100"`
	Descr         string   `form:"descr" binding:"min=2,max=255"`
	Content       string   `form:"content" binding:"min=2,max=4294967295"`
	CoverImageUrl string   `form:"cover_image_url" binding:"url"`
	ModifiedBy    string   `form:"modified_by" binding:"required,min=2,max=100"`
	State         uint8    `form:"state,default=1" binding:"oneof=0 1"`
}

type DeleteArticleRequest struct {
	ID uint32 `form:"id" binding:"required,gte=1"`
}

type Article struct {
	ID            uint32       `json:"id"`
	Title         string       `json:"title"`
	Descr         string       `json:"descr"`
	Content       string       `json:"content"`
	CoverImageUrl string       `json:"cover_image_url"`
	State         uint8        `json:"state"`
	Tag           []*model.Tag `json:"tag"`
}

func (svc *Service) GetArticle(param *ArticleRequest) (*Article, error) {
	article, err := svc.dao.GetArticle(param.ID, param.State)
	if err != nil {
		return nil, err
	}

	articleTag, err := svc.dao.GetArticleTagByAID(article.ID)
	if err != nil {
		return nil, err
	}

	var tags []*model.Tag
	for _, at := range articleTag {
		tag, err := svc.dao.GetTag(at.TagID, 1)
		if err != nil {
			return nil, err
		}
		tags = append(tags, &tag)
	}

	return &Article{
		ID:            article.ID,
		Title:         article.Title,
		Descr:         article.Descr,
		Content:       article.Content,
		CoverImageUrl: article.CoverImageUrl,
		State:         article.State,
		Tag:           tags,
	}, nil
}

func (svc *Service) GetArticleList(param *ArticleListRequest, pager *app.Pager) ([]*Article, int, error) {
	articleCount, err := svc.dao.CountArticleListByTagID(param.TagID, param.State)
	if err != nil {
		return nil, 0, err
	}

	articles, err := svc.dao.GetArticleListByTagID(param.TagID, param.State, pager.Page, pager.PageSize)
	if err != nil {
		return nil, 0, err
	}

	var articleList []*Article
	for _, article := range articles {
		articleList = append(articleList, &Article{
			ID:            article.ArticleID,
			Title:         article.ArticleTitle,
			Descr:         article.ArticleDesc,
			Content:       article.Content,
			CoverImageUrl: article.CoverImageUrl,
			//Tag:           &model.Tag{Model: &model.Model{ID: article.TagID}, Name: article.TagName},
		})
	}

	return articleList, articleCount, nil
}

func (svc *Service) CreateArticle(param *CreateArticleRequest) error {
	article, err := svc.dao.CreateArticle(&dao.Article{
		Title:         param.Title,
		Descr:         param.Descr,
		Content:       param.Content,
		CoverImageUrl: param.CoverImageUrl,
		State:         param.State,
		CreatedBy:     param.CreateBy,
	})
	if err != nil {
		return err
	}

	tagsStr := strings.Split(param.TagID, ",")
	var tags []uint32
	for _, val := range tagsStr {
		tagID := convert.StrTo(val).MustUInt32()
		tags = append(tags, tagID)
	}
	err = svc.dao.CreateArticleTag(article.ID, tags, article.CreatedBy)
	if err != nil {
		return err
	}

	return nil
}

func (svc *Service) UpdateArticle(param *UpdateArticleRequest) error {
	err := svc.dao.UpdateArticle(&dao.Article{
		ID:            param.ID,
		Title:         param.Title,
		Descr:         param.Descr,
		Content:       param.Content,
		CoverImageUrl: param.CoverImageUrl,
		State:         param.State,
		ModifiedBy:    param.ModifiedBy,
	})
	if err != nil {
		return err
	}

	err = svc.dao.UpdateArticleTag(param.ID, param.TagID, param.ModifiedBy)
	if err != nil {
		return err
	}

	return nil
}

func (svc *Service) DeleteArticle(param *DeleteArticleRequest) error {
	if err := svc.dao.DeleteArticle(param.ID); err != nil {
		return err
	}

	if err := svc.dao.DeleteArticleTag(param.ID); err != nil {
		return err
	}

	return nil
}
