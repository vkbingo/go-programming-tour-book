package main

import (
	"flag"
	"fmt"
	"log"
)

var name string

func main() {
	flag.Parse()
	args := flag.Args()
	fmt.Println(args)
	if len(args) <= 0 {
		return
	}

	switch args[0] {
	case "go":
		goCmd := flag.NewFlagSet("go", flag.ExitOnError)
		goCmd.StringVar(&name, "name", "Go 语言", "help info")
		goCmd.Parse(args[1:])
	case "php":
		phpCmd := flag.NewFlagSet("php", flag.ExitOnError)
		phpCmd.StringVar(&name, "name", "PHP语言", "help info")
		fmt.Println(args)
		fmt.Println(args[1][2:])
		phpCmd.Parse(args[1:])
	}
	log.Printf("name: %s", name)
}
