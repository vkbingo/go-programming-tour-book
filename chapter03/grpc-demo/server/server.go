package main

import (
	"context"
	"flag"
	pb "github.com/go-programming-tour-book/grpc-demo/proto"
	"google.golang.org/grpc"
	"io"
	"log"
	"net"
)

var port string

func init() {
	flag.StringVar(&port, "port", "8004", "启动端口号")
	flag.Parse()
}

type GreeterServer struct {
}

func (s *GreeterServer) SayHello(ctx context.Context, r *pb.HelloRequest) (*pb.HelloReply, error) {
	return &pb.HelloReply{Message: "hello world! allin"}, nil
}

type Greeter1Server struct {
}

func (s *Greeter1Server) SayList(r *pb.HelloRequest, stream pb.Greeter1_SayListServer) error {
	for n := 0; n <= 6; n++ {
		_ = stream.Send(&pb.HelloReply{Message: "Hello.list"})
	}
	return nil
}

type Greeter2Server struct {
}

func (s *Greeter2Server) SayRecord(stream pb.Greeter2_SayRecordServer) error {
	for {
		resp, err := stream.Recv()
		if err == io.EOF {
			message := &pb.HelloReply{Message: "say.record"}
			return stream.SendAndClose(message)
		}
		if err != nil {
			return err
		}
		log.Printf("resp: %v", resp)
	}
	return nil
}

type Greeter3Server struct {
}

func (s *Greeter3Server) SayRoute(stream pb.Greeter3_SayRouteServer) error {
	n := 0
	for {
		_ = stream.Send(&pb.HelloReply{Message: "say.rrrrrrrrrrrrrrrrrrrrr"})
		resp, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}

		n++
		log.Printf("resp: %v", resp)
	}
}

func main() {
	//server := grpc.NewServer()
	//pb.RegisterGreeterServer(server, &GreeterServer{})
	//lis, _ := net.Listen("tcp", ":"+port)
	//fmt.Println(lis)
	//server.Serve(lis)

	//server := grpc.NewServer()
	//pb.RegisterGreeter1Server(server, &Greeter1Server{})
	//lis, _ := net.Listen("tcp", ":"+port)
	//server.Serve(lis)

	//server := grpc.NewServer()
	//pb.RegisterGreeter2Server(server, &Greeter2Server{})
	//lis, _ := net.Listen("tcp", ":"+port)
	//server.Serve(lis)

	server := grpc.NewServer()
	pb.RegisterGreeter3Server(server, &Greeter3Server{})
	lis, _ := net.Listen("tcp", ":"+port)
	server.Serve(lis)
}
