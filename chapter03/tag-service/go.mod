module github.com/go-programming-tour-book/tag-service

go 1.16

require (
	github.com/elazarl/go-bindata-assetfs v1.0.1 // indirect
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/soheilhy/cmux v0.1.4
	golang.org/x/net v0.0.0-20210917221730-978cfadd31cf
	golang.org/x/sys v0.0.0-20210921065528-437939a70204 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20210920155426-26f343e4c215
	google.golang.org/grpc v1.40.0
	google.golang.org/grpc/examples v0.0.0-20210917221926-1109452fd118 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
