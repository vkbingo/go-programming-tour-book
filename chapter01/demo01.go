package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	var name string
	fmt.Println("1", os.Args[0])
	fmt.Println("2", os.Args[1:])
	flag.StringVar(&name, "name", "go语言编程之旅", "help info")
	flag.StringVar(&name, "n", "go语言编程之旅1211", "help info")
	flag.Parse()
	fmt.Println(flag.Args())

	log.Printf("name: %s", name)
}
