package main

import (
	"fmt"
	"time"
)

func main() {
	location, _ := time.LoadLocation("Asia/Shanghai")
	inputTime := "2029-09-04 15:16:17"
	layout := "2006-01-02 15:04:05"
	t, _ := time.ParseInLocation(layout, inputTime, location)
	dateTime := time.Unix(t.Unix(), 0).In(location).Format(layout)
	fmt.Println(inputTime, dateTime)
}

type Servers struct {
	// Server_name
	ServerName     string  `json:&#34;Server_name&#34;`
	// Host
	Host   string  `json:&#34;Host&#34;`
	// Db
	Db     string  `json:&#34;Db&#34;`
	// Username
	Username       string  `json:&#34;Username&#34;`
	// Password
	Password       string  `json:&#34;Password&#34;`
	// Port
	Port   int32   `json:&#34;Port&#34;`
	// Socket
	Socket string  `json:&#34;Socket&#34;`
	// Wrapper
	Wrapper        string  `json:&#34;Wrapper&#34;`
	// Owner
	Owner  string  `json:&#34;Owner&#34;`
}

func (model Servers) TableName() string {
	return "servers"
}

