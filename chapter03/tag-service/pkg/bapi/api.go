package bapi

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type API struct {
	URL    string
	Client *http.Client
}

const (
	APP_KEY    = "allin"
	APP_SECRET = "go-programming-tour-book"
)

func NewAPI(url string) *API {
	return &API{URL: url}
}

func (a *API) GetTagList(ctx context.Context, name string) ([]byte, error) {
	token, err := a.getAccessToken(ctx)
	if err != nil {
		return nil, err
	}

	body, err := a.httpGet(ctx, fmt.Sprintf(
		"%s?token=%s&name=%s",
		"api/v1/tags",
		token,
		name,
	))
	if err != nil {
		return nil, err
	}

	return body, nil
}

type AccessToken struct {
	Token string `json:"token"`
}

func (a *API) getAccessToken(ctx context.Context) (string, error) {
	tmp := struct {
		AppKey    string `json:"AppKey"`
		AppSecret string `json:"AppSecret"`
	}{
		AppKey:    APP_KEY,
		AppSecret: APP_SECRET,
	}
	data, _ := json.Marshal(tmp)
	url := fmt.Sprintf("%s/%s", a.URL, "auth")
	body, err := a.httpPost(ctx, url, data)
	if err != nil {
		return "", err
	}

	var accessToken AccessToken
	_ = json.Unmarshal(body, &accessToken)
	return accessToken.Token, nil
}

func (a *API) httpGet(ctx context.Context, path string) ([]byte, error) {
	resp, err := http.Get(fmt.Sprintf("%s/%s", a.URL, path))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	return body, nil
}

func (a *API) httpPost(ctx context.Context, url string, data []byte) ([]byte, error) {
	resp, err := http.Post(url, "application/json", bytes.NewReader(data))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}
