// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/helloworld.proto

package helloworld

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type HelloRequest struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *HelloRequest) Reset()         { *m = HelloRequest{} }
func (m *HelloRequest) String() string { return proto.CompactTextString(m) }
func (*HelloRequest) ProtoMessage()    {}
func (*HelloRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_4d53fe9c48eadaad, []int{0}
}

func (m *HelloRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HelloRequest.Unmarshal(m, b)
}
func (m *HelloRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HelloRequest.Marshal(b, m, deterministic)
}
func (m *HelloRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HelloRequest.Merge(m, src)
}
func (m *HelloRequest) XXX_Size() int {
	return xxx_messageInfo_HelloRequest.Size(m)
}
func (m *HelloRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_HelloRequest.DiscardUnknown(m)
}

var xxx_messageInfo_HelloRequest proto.InternalMessageInfo

func (m *HelloRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type HelloReply struct {
	Message              string   `protobuf:"bytes,1,opt,name=message,proto3" json:"message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *HelloReply) Reset()         { *m = HelloReply{} }
func (m *HelloReply) String() string { return proto.CompactTextString(m) }
func (*HelloReply) ProtoMessage()    {}
func (*HelloReply) Descriptor() ([]byte, []int) {
	return fileDescriptor_4d53fe9c48eadaad, []int{1}
}

func (m *HelloReply) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HelloReply.Unmarshal(m, b)
}
func (m *HelloReply) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HelloReply.Marshal(b, m, deterministic)
}
func (m *HelloReply) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HelloReply.Merge(m, src)
}
func (m *HelloReply) XXX_Size() int {
	return xxx_messageInfo_HelloReply.Size(m)
}
func (m *HelloReply) XXX_DiscardUnknown() {
	xxx_messageInfo_HelloReply.DiscardUnknown(m)
}

var xxx_messageInfo_HelloReply proto.InternalMessageInfo

func (m *HelloReply) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func init() {
	proto.RegisterType((*HelloRequest)(nil), "helloworld.HelloRequest")
	proto.RegisterType((*HelloReply)(nil), "helloworld.HelloReply")
}

func init() { proto.RegisterFile("proto/helloworld.proto", fileDescriptor_4d53fe9c48eadaad) }

var fileDescriptor_4d53fe9c48eadaad = []byte{
	// 208 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x2b, 0x28, 0xca, 0x2f,
	0xc9, 0xd7, 0xcf, 0x48, 0xcd, 0xc9, 0xc9, 0x2f, 0xcf, 0x2f, 0xca, 0x49, 0xd1, 0x03, 0x0b, 0x08,
	0x71, 0x21, 0x44, 0x94, 0x94, 0xb8, 0x78, 0x3c, 0x40, 0xbc, 0xa0, 0xd4, 0xc2, 0xd2, 0xd4, 0xe2,
	0x12, 0x21, 0x21, 0x2e, 0x96, 0xbc, 0xc4, 0xdc, 0x54, 0x09, 0x46, 0x05, 0x46, 0x0d, 0xce, 0x20,
	0x30, 0x5b, 0x49, 0x8d, 0x8b, 0x0b, 0xaa, 0xa6, 0x20, 0xa7, 0x52, 0x48, 0x82, 0x8b, 0x3d, 0x37,
	0xb5, 0xb8, 0x38, 0x31, 0x1d, 0xa6, 0x08, 0xc6, 0x35, 0xf2, 0xe4, 0x62, 0x77, 0x2f, 0x4a, 0x4d,
	0x2d, 0x49, 0x2d, 0x12, 0xb2, 0xe3, 0xe2, 0x08, 0x4e, 0xac, 0x04, 0xeb, 0x12, 0x92, 0xd0, 0x43,
	0x72, 0x01, 0xb2, 0x65, 0x52, 0x62, 0x58, 0x64, 0x0a, 0x72, 0x2a, 0x95, 0x18, 0x8c, 0xbc, 0xb9,
	0x38, 0xa0, 0x46, 0x19, 0x0a, 0xd9, 0x73, 0xb1, 0x07, 0x27, 0x56, 0xfa, 0x64, 0x16, 0x97, 0x90,
	0x63, 0x94, 0x01, 0xa3, 0x91, 0x2f, 0xdc, 0x30, 0x23, 0x21, 0x47, 0x2e, 0xce, 0xe0, 0xc4, 0xca,
	0xa0, 0xd4, 0xe4, 0xfc, 0xa2, 0x14, 0x72, 0x8c, 0xd3, 0x60, 0x34, 0xf2, 0x83, 0x1b, 0x67, 0x2c,
	0xe4, 0x04, 0xf6, 0x67, 0x50, 0x7e, 0x69, 0x49, 0x2a, 0x79, 0xa6, 0x19, 0x30, 0x26, 0xb1, 0x81,
	0x63, 0xc5, 0x18, 0x10, 0x00, 0x00, 0xff, 0xff, 0xbe, 0x40, 0xc2, 0xf2, 0xaf, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// GreeterClient is the client API for Greeter service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type GreeterClient interface {
	SayHello(ctx context.Context, in *HelloRequest, opts ...grpc.CallOption) (*HelloReply, error)
}

type greeterClient struct {
	cc grpc.ClientConnInterface
}

func NewGreeterClient(cc grpc.ClientConnInterface) GreeterClient {
	return &greeterClient{cc}
}

func (c *greeterClient) SayHello(ctx context.Context, in *HelloRequest, opts ...grpc.CallOption) (*HelloReply, error) {
	out := new(HelloReply)
	err := c.cc.Invoke(ctx, "/helloworld.Greeter/SayHello", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// GreeterServer is the server API for Greeter service.
type GreeterServer interface {
	SayHello(context.Context, *HelloRequest) (*HelloReply, error)
}

// UnimplementedGreeterServer can be embedded to have forward compatible implementations.
type UnimplementedGreeterServer struct {
}

func (*UnimplementedGreeterServer) SayHello(ctx context.Context, req *HelloRequest) (*HelloReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SayHello not implemented")
}

func RegisterGreeterServer(s *grpc.Server, srv GreeterServer) {
	s.RegisterService(&_Greeter_serviceDesc, srv)
}

func _Greeter_SayHello_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HelloRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GreeterServer).SayHello(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/helloworld.Greeter/SayHello",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GreeterServer).SayHello(ctx, req.(*HelloRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Greeter_serviceDesc = grpc.ServiceDesc{
	ServiceName: "helloworld.Greeter",
	HandlerType: (*GreeterServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SayHello",
			Handler:    _Greeter_SayHello_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto/helloworld.proto",
}

// Greeter1Client is the client API for Greeter1 service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type Greeter1Client interface {
	SayList(ctx context.Context, in *HelloRequest, opts ...grpc.CallOption) (Greeter1_SayListClient, error)
}

type greeter1Client struct {
	cc grpc.ClientConnInterface
}

func NewGreeter1Client(cc grpc.ClientConnInterface) Greeter1Client {
	return &greeter1Client{cc}
}

func (c *greeter1Client) SayList(ctx context.Context, in *HelloRequest, opts ...grpc.CallOption) (Greeter1_SayListClient, error) {
	stream, err := c.cc.NewStream(ctx, &_Greeter1_serviceDesc.Streams[0], "/helloworld.Greeter1/SayList", opts...)
	if err != nil {
		return nil, err
	}
	x := &greeter1SayListClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type Greeter1_SayListClient interface {
	Recv() (*HelloReply, error)
	grpc.ClientStream
}

type greeter1SayListClient struct {
	grpc.ClientStream
}

func (x *greeter1SayListClient) Recv() (*HelloReply, error) {
	m := new(HelloReply)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// Greeter1Server is the server API for Greeter1 service.
type Greeter1Server interface {
	SayList(*HelloRequest, Greeter1_SayListServer) error
}

// UnimplementedGreeter1Server can be embedded to have forward compatible implementations.
type UnimplementedGreeter1Server struct {
}

func (*UnimplementedGreeter1Server) SayList(req *HelloRequest, srv Greeter1_SayListServer) error {
	return status.Errorf(codes.Unimplemented, "method SayList not implemented")
}

func RegisterGreeter1Server(s *grpc.Server, srv Greeter1Server) {
	s.RegisterService(&_Greeter1_serviceDesc, srv)
}

func _Greeter1_SayList_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(HelloRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(Greeter1Server).SayList(m, &greeter1SayListServer{stream})
}

type Greeter1_SayListServer interface {
	Send(*HelloReply) error
	grpc.ServerStream
}

type greeter1SayListServer struct {
	grpc.ServerStream
}

func (x *greeter1SayListServer) Send(m *HelloReply) error {
	return x.ServerStream.SendMsg(m)
}

var _Greeter1_serviceDesc = grpc.ServiceDesc{
	ServiceName: "helloworld.Greeter1",
	HandlerType: (*Greeter1Server)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "SayList",
			Handler:       _Greeter1_SayList_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "proto/helloworld.proto",
}

// Greeter2Client is the client API for Greeter2 service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type Greeter2Client interface {
	SayRecord(ctx context.Context, opts ...grpc.CallOption) (Greeter2_SayRecordClient, error)
}

type greeter2Client struct {
	cc grpc.ClientConnInterface
}

func NewGreeter2Client(cc grpc.ClientConnInterface) Greeter2Client {
	return &greeter2Client{cc}
}

func (c *greeter2Client) SayRecord(ctx context.Context, opts ...grpc.CallOption) (Greeter2_SayRecordClient, error) {
	stream, err := c.cc.NewStream(ctx, &_Greeter2_serviceDesc.Streams[0], "/helloworld.Greeter2/SayRecord", opts...)
	if err != nil {
		return nil, err
	}
	x := &greeter2SayRecordClient{stream}
	return x, nil
}

type Greeter2_SayRecordClient interface {
	Send(*HelloRequest) error
	CloseAndRecv() (*HelloReply, error)
	grpc.ClientStream
}

type greeter2SayRecordClient struct {
	grpc.ClientStream
}

func (x *greeter2SayRecordClient) Send(m *HelloRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *greeter2SayRecordClient) CloseAndRecv() (*HelloReply, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(HelloReply)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// Greeter2Server is the server API for Greeter2 service.
type Greeter2Server interface {
	SayRecord(Greeter2_SayRecordServer) error
}

// UnimplementedGreeter2Server can be embedded to have forward compatible implementations.
type UnimplementedGreeter2Server struct {
}

func (*UnimplementedGreeter2Server) SayRecord(srv Greeter2_SayRecordServer) error {
	return status.Errorf(codes.Unimplemented, "method SayRecord not implemented")
}

func RegisterGreeter2Server(s *grpc.Server, srv Greeter2Server) {
	s.RegisterService(&_Greeter2_serviceDesc, srv)
}

func _Greeter2_SayRecord_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(Greeter2Server).SayRecord(&greeter2SayRecordServer{stream})
}

type Greeter2_SayRecordServer interface {
	SendAndClose(*HelloReply) error
	Recv() (*HelloRequest, error)
	grpc.ServerStream
}

type greeter2SayRecordServer struct {
	grpc.ServerStream
}

func (x *greeter2SayRecordServer) SendAndClose(m *HelloReply) error {
	return x.ServerStream.SendMsg(m)
}

func (x *greeter2SayRecordServer) Recv() (*HelloRequest, error) {
	m := new(HelloRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

var _Greeter2_serviceDesc = grpc.ServiceDesc{
	ServiceName: "helloworld.Greeter2",
	HandlerType: (*Greeter2Server)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "SayRecord",
			Handler:       _Greeter2_SayRecord_Handler,
			ClientStreams: true,
		},
	},
	Metadata: "proto/helloworld.proto",
}

// Greeter3Client is the client API for Greeter3 service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type Greeter3Client interface {
	SayRoute(ctx context.Context, opts ...grpc.CallOption) (Greeter3_SayRouteClient, error)
}

type greeter3Client struct {
	cc grpc.ClientConnInterface
}

func NewGreeter3Client(cc grpc.ClientConnInterface) Greeter3Client {
	return &greeter3Client{cc}
}

func (c *greeter3Client) SayRoute(ctx context.Context, opts ...grpc.CallOption) (Greeter3_SayRouteClient, error) {
	stream, err := c.cc.NewStream(ctx, &_Greeter3_serviceDesc.Streams[0], "/helloworld.Greeter3/SayRoute", opts...)
	if err != nil {
		return nil, err
	}
	x := &greeter3SayRouteClient{stream}
	return x, nil
}

type Greeter3_SayRouteClient interface {
	Send(*HelloRequest) error
	Recv() (*HelloReply, error)
	grpc.ClientStream
}

type greeter3SayRouteClient struct {
	grpc.ClientStream
}

func (x *greeter3SayRouteClient) Send(m *HelloRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *greeter3SayRouteClient) Recv() (*HelloReply, error) {
	m := new(HelloReply)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// Greeter3Server is the server API for Greeter3 service.
type Greeter3Server interface {
	SayRoute(Greeter3_SayRouteServer) error
}

// UnimplementedGreeter3Server can be embedded to have forward compatible implementations.
type UnimplementedGreeter3Server struct {
}

func (*UnimplementedGreeter3Server) SayRoute(srv Greeter3_SayRouteServer) error {
	return status.Errorf(codes.Unimplemented, "method SayRoute not implemented")
}

func RegisterGreeter3Server(s *grpc.Server, srv Greeter3Server) {
	s.RegisterService(&_Greeter3_serviceDesc, srv)
}

func _Greeter3_SayRoute_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(Greeter3Server).SayRoute(&greeter3SayRouteServer{stream})
}

type Greeter3_SayRouteServer interface {
	Send(*HelloReply) error
	Recv() (*HelloRequest, error)
	grpc.ServerStream
}

type greeter3SayRouteServer struct {
	grpc.ServerStream
}

func (x *greeter3SayRouteServer) Send(m *HelloReply) error {
	return x.ServerStream.SendMsg(m)
}

func (x *greeter3SayRouteServer) Recv() (*HelloRequest, error) {
	m := new(HelloRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

var _Greeter3_serviceDesc = grpc.ServiceDesc{
	ServiceName: "helloworld.Greeter3",
	HandlerType: (*Greeter3Server)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "SayRoute",
			Handler:       _Greeter3_SayRoute_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
	},
	Metadata: "proto/helloworld.proto",
}
