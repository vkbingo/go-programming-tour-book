package main

import (
	"context"
	"errors"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

func Rpc(ctx context.Context, url string) error {
	result := make(chan int)
	err := make(chan error)

	go func() {
		// 进行RPC调用，并且返回是否成功，成功通过result传递成功信息，错误通过error传递错误信息
		isSuccess := true
		if isSuccess {
			result <- 1
		} else {
			err <- errors.New("some error happen")
		}
	}()

	select {
	case <- ctx.Done():
		// 其他RPC调用调用失败
		return ctx.Err()
	case e := <- err:
		// 本RPC调用失败，返回错误信息
		return e
	case <- result:
		// 本RPC调用成功，不返回错误信息
		return nil
	}
}


func main() {
	file, _ := exec.LookPath(os.Args[0])
	path, _ := filepath.Abs(file)
	log.Println(path)
}

//docker run -d --name jaeger \
//-e COLLECTOR_ZIPKIN_HTTP_PORT=9411 \
//-p 5775:5775/udp \
//-p 6831:6831/udp \
//-p 6832:6832/udp \
//-p 5778:5778 \
//-p 16686:16686 \
//-p 14268:14268 \
//-p 9411:9411 \
//jaegertracing/all-in-one
//
//kubectl run kube-jaeger --image=jaegertracing/all-in-one:latest \
//-e COLLECTOR_ZIPKIN_HTTP_PORT=9411 \
//-p 5775:5775/udp \
//-p 6831:6831/udp \
//-p 6832:6832/udp \
//-p 5778:5778 \
//-p 16686:16686 \
//-p 14268:14268 \
//-p 9411:9411